package entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class Loans {
    private long loanId;
    private LocalDateTime createdAt;
    private long typeId;
    private String description;
    private List<Transaction> transactionList;
    private double price;
    private LocalDate dateEnd;
    private float percent;
}
