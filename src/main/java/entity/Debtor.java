package entity;

import java.util.List;

public class Debtor {
    private String debId;
    private String debName;
    private String phone;
    private String email;
    private String address;
    private List<Loans> loansList;
    private long cusId;
}
