package entity;

import java.time.LocalDateTime;
import java.util.List;


public class Customer {
    private long cusId;
    private String userName;
    private String passWord;
    private String email;
    private String fullName;
    private String phone;
    private boolean checked;
    private LocalDateTime createdAt;
    private List<Loans> loansList;
    private List<Debtor> debtorList;
}
